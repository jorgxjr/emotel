package com.emotel.core.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.emotel.core.entities.ReservaHotel;




@Service
public interface IReservaHotelService {
	public boolean agregar(ReservaHotel reservahotel);
	public List<ReservaHotel> listar();
	public boolean eliminar(int id);
	public ReservaHotel cargarReservaHotel(int id);
}

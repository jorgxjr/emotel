package com.emotel.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.emotel.core.entities.ReservaHotel;


@Repository
public interface IReservaHotelRepository extends JpaRepository<ReservaHotel, Integer>{

}

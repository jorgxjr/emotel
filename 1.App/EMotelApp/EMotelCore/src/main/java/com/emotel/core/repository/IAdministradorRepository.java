package com.emotel.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.emotel.core.entities.Administrador;



@Repository
public interface IAdministradorRepository extends JpaRepository<Administrador, Integer>{

	public Administrador buscarPorCorreo(String correo,String contraseña);
	
	public boolean existeCorreo(String correo);
}

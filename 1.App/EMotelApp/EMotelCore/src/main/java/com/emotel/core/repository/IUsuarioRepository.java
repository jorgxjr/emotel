package com.emotel.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.emotel.core.entities.Usuario;



@Repository
public interface IUsuarioRepository extends JpaRepository<Usuario, Integer>{
	public Usuario buscarPorCorreo(String correo, String password);
	
	public Usuario existeCorreo(String correo);
}

package com.emotel.core.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.emotel.core.entities.ReservaHotel;
import com.emotel.core.repository.IReservaHotelRepository;
@Service
public class ReservaHotelService implements IReservaHotelService {
	@Autowired
	IReservaHotelRepository repository;
	public boolean agregar(ReservaHotel reservahotel) {
		boolean flag = false;
		try {
			ReservaHotel objReservaHotel= repository.save(reservahotel);
			if(objReservaHotel!= null) {
				flag = true;
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return flag;
	}

	public List<ReservaHotel> listar() {
		return repository.findAll();
	}

	public boolean eliminar(int id) {
		boolean flag = false;
		try {
			repository.delete(id);
			flag = true;
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}
		return flag;
	}

	public ReservaHotel cargarReservaHotel(int id) {
		ReservaHotel objReserva = null;
		try {
			objReserva = repository.getOne(id);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}
		return objReserva;
	}

}

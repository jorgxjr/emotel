//<<<<<<< HEAD
//package com.emotel.core.unit.test;
//
//import org.junit.FixMethodOrder;
//import org.junit.experimental.categories.Category;
//import org.junit.runner.RunWith;
//import org.junit.runners.MethodSorters;
//import org.mockito.junit.MockitoJUnitRunner;
//
//import com.emotel.core.entities.ReservaRestaurante;
//import com.emotel.core.service.IReservaRestauranteService;
//import com.emotel.core.unit.suite.PruebasUnitariasTest;
//
//@Category(PruebasUnitariasTest.class)
//@RunWith(MockitoJUnitRunner.Silent.class)
//@FixMethodOrder(MethodSorters.NAME_ASCENDING)
//public class ReservaRestauranteTest {
//
//	private ReservaRestaurante reservarestaurante;
//	
//	private IReservaRestauranteService reservarestauranteService;
//}
//=======
package com.emotel.core.unit.test;

import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.emotel.core.entities.ReservaRestaurante;
import com.emotel.core.entities.TipoHotel;
import com.emotel.core.service.IReservaRestauranteService;
import com.emotel.core.unit.suite.PruebasUnitariasTest;

@Category(PruebasUnitariasTest.class)
@RunWith(MockitoJUnitRunner.Silent.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ReservaRestauranteTest {

	@Mock
	private ReservaRestaurante reservarestaurante;
	
	@Mock
	private IReservaRestauranteService reservarestauranteService;
	
	@BeforeClass
    public static void inicioClase() {
        System.out.println("Inicio de la clase");
    }

	@AfterClass
    public static void finClase() {
        System.out.println("Fin de la clase");
    }

	@Before
    public void inicioMetodo() {
        System.out.println("Inicio Metodo");
    }

	@After
    public void finMetodo() {
        System.out.println("Fin Metodo");
    }
	
	@Test
    public void a_insertar() {
    	try {
    		System.out.println("Método Insertar");
    		when(reservarestaurante.getId()).thenReturn(1);
    		when(reservarestaurante.getHora()).thenReturn("5 tarde");
    		when(reservarestaurante.getFecha()).thenReturn(null);
    		when(reservarestauranteService.agregar(ArgumentMatchers.any())).thenReturn(true);
    		reservarestauranteService.agregar(reservarestaurante);
    		Assert.assertTrue(reservarestaurante.getId() > 0);
		} catch (Exception e) {
			e.printStackTrace();
            Assert.fail();
		}
    }
	
	 @Test
	    public void b_listar() {
	    	try {
	    		System.out.println("Método Listar");
	    		List<ReservaRestaurante> listaReservaRestaurante = spy(new ArrayList<ReservaRestaurante>());
	    		when(listaReservaRestaurante.add(ArgumentMatchers.any())).thenReturn(true);
	    		when(listaReservaRestaurante.add(ArgumentMatchers.any())).thenReturn(true);
	    		when(listaReservaRestaurante.add(ArgumentMatchers.any())).thenReturn(true);
	    		when(reservarestauranteService.listar()).thenReturn(listaReservaRestaurante);
	    		List<ReservaRestaurante> lista = reservarestauranteService.listar();
	    		Assert.assertTrue(lista.size() > 0);
			} catch (Exception e) {
				e.printStackTrace();
	            Assert.fail();
			}
	    }
	  @Test
	    public void c_eliminar() {
	    	try {
	    		System.out.println("Método Eliminar");
	    		when(reservarestauranteService.eliminar(reservarestaurante.getId())).thenReturn(true);
	    		reservarestauranteService.eliminar(reservarestaurante.getId());
	    		Assert.assertTrue(true);
			} catch (Exception e) {
				e.printStackTrace();
	            Assert.fail();
			}
	    }
	    
    
}
//>>>>>>> 2a956fbc6e301c9dfbc150313a9f7cdcf8c238db

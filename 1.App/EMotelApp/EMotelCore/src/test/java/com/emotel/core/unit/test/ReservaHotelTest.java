//<<<<<<< HEAD
//package com.emotel.core.unit.test;
//
//import org.junit.FixMethodOrder;
//import org.junit.experimental.categories.Category;
//import org.junit.runner.RunWith;
//import org.junit.runners.MethodSorters;
//import org.mockito.Mock;
//import org.mockito.junit.MockitoJUnitRunner;
//
//import com.emotel.core.entities.ReservaHotel;
//import com.emotel.core.service.IReservaHotelService;
//import com.emotel.core.unit.suite.PruebasUnitariasTest;
//
//@Category(PruebasUnitariasTest.class)
//@RunWith(MockitoJUnitRunner.Silent.class)
//@FixMethodOrder(MethodSorters.NAME_ASCENDING)
//public class ReservaHotelTest {
//	
//	@Mock
//	private ReservaHotel reservahotel;
//	
//	@Mock
//	private IReservaHotelService reservahotelService;
//	
//}
//=======
package com.emotel.core.unit.test;

import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.emotel.core.entities.Hotel;
import com.emotel.core.entities.ReservaHotel;
import com.emotel.core.service.IHotelService;
import com.emotel.core.service.IReservaHotelService;
import com.emotel.core.unit.suite.PruebasUnitariasTest;

@Category(PruebasUnitariasTest.class)
@RunWith(MockitoJUnitRunner.Silent.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ReservaHotelTest {
	
	@Mock
	private ReservaHotel reservahotel;
	
	@Mock
	private IReservaHotelService reservahotelService;
	
	@BeforeClass
    public static void inicioClase() {
        System.out.println("Inicio de la clase");
    }

	@AfterClass
    public static void finClase() {
        System.out.println("Fin de la clase");
    }

	@Before
    public void inicioMetodo() {
        System.out.println("Inicio Metodo");
    }

	@After
    public void finMetodo() {
        System.out.println("Fin Metodo");
    }
    
	@Test
    public void a_insertar() {
      try {
        System.out.println("Método Insertar");
        when(reservahotel.getId()).thenReturn(1);
        when(reservahotel.getNrodias()).thenReturn(3);
        when(reservahotel.getNrocuartos()).thenReturn(4);
        when(reservahotel.getNropersonas()).thenReturn(15);
        when(reservahotelService.agregar(ArgumentMatchers.any())).thenReturn(true);
        reservahotelService.agregar(reservahotel);
        Assert.assertTrue(reservahotel.getId() > 0);
    } catch (Exception e) {
      e.printStackTrace();
            Assert.fail();
    		}
    }
	
}
//>>>>>>> 2a956fbc6e301c9dfbc150313a9f7cdcf8c238db

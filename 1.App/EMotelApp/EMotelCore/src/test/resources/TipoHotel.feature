Feature: Mantenimiento de Tipo de Hotel
	Como administrador necesito realizar la gestión de Tipos de Hotel
	
	@tag1
	Scenario: Registro de Tipo de Hotel
		Given el administrador se loggea a la aplicacion "emotel"
		When da clic en la seccion "Tipo Hotel"
		And se redirecciona a la vista del "Registro" en Tipo de Hotel
		And en la pantalla escribo en campo Nombre el valor "Premium"
		And en la pantalla escribo en campo Descripcion el valor "Alta calidad" 
		And da clic en "Registrar"
		Then guarda en la base de datos el nuevo Tipo de Hotel
		
	
	@tag2
	Scenario: Eliminar Tipo de Hotel
		Given el administrador se loggea a la aplicacion "emotel"
		When da clic en la seccion "Tipo Hotel"
		And da clic al boton "Eliminar"
		Then se elimina el tipo de hotel de la base de datos
	